# file      : buildfile
# copyright : Copyright (c) 2016-2019 Code Synthesis Ltd
# license   : ISC; see accompanying COPYING file

./: {*/ -build/} doc{AUTHORS COPYING INSTALL README} manifest

# Don't install tests or the INSTALL file.
#
tests/:          install = false
doc{INSTALL}@./: install = false
