/* file      : libpkgconf/config.h -*- C -*-
 * copyright : Copyright (c) 2016-2019 Code Synthesis Ltd
 * license   : ISC; see accompanying COPYING file
 */

/*
 * Defines PACKAGE_VERSION.
 */
#include <libpkgconf/version.h>

/*
 * strndup() is not present on Windows, for gcc and clang if compile with
 * -std=C99, except for FreeBSD and MacOS.
 *
 * strl*() are only present on FreeBSD and MacOS.
 */
#if defined(__FreeBSD__) || defined(__APPLE__)
#  define HAVE_STRNDUP 1
#  define HAVE_STRLCPY 1
#  define HAVE_STRLCAT 1
#endif

/*
 * Let's assume cygwin_conv_path() is always available if compile with MSYS
 * gcc.
 */
#if defined(__MSYS__)
# define HAVE_CYGWIN_CONV_PATH 1
#endif

/*
 * We don't consider such an outdated environments.
 */
#define HAVE_SYS_STAT_H 1

#define PACKAGE_BUGREPORT "https://git.dereferenced.org/pkgconf/pkgconf/issues"
